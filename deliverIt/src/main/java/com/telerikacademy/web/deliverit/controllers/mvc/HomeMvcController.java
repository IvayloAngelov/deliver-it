package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.User;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class HomeMvcController extends BasicAuthenticationMvcController {

    private final CustomerService customerService;
    private final UserRepository userRepository;
    private final ParcelService parcelService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper,
                             CustomerService customerService,
                             UserRepository userRepository,
                             ParcelService parcelService) {
        super(authenticationHelper);
        this.customerService = customerService;
        this.userRepository = userRepository;
        this.parcelService = parcelService;
    }

    @GetMapping
    public String showHomePage(Model model) {
        int numberOfCustomers = customerService.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty()).size();
        model.addAttribute("customers", numberOfCustomers);
        return "index";
    }

}
