package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.services.contracts.*;
import com.telerikacademy.web.deliverit.services.modelMappers.ParcelModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.web.deliverit.controllers.AuthenticationHelper.EMPLOYEE_ROLE;


@Controller
@RequestMapping("/parcels")
public class ParcelMvcController extends BasicAuthenticationMvcController{
    private final ParcelService parcelService;
    private final AuthenticationHelper authenticationHelper;
    private final ShipmentService shipmentService;
    private final WarehouseService warehouseService;
    private final CategoryService categoryService;
    private final CustomerService customerService;
    private final ParcelModelMapper parcelModelMapper;

    @Autowired
    public ParcelMvcController(ParcelService parcelService,
                               AuthenticationHelper authenticationHelper,
                               ShipmentService shipmentService,
                               WarehouseService warehouseService,
                               CategoryService categoryService,
                               CustomerService customerService,
                               ParcelModelMapper parcelModelMapper) {
        super(authenticationHelper);
        this.parcelService = parcelService;
        this.authenticationHelper = authenticationHelper;
        this.shipmentService = shipmentService;
        this.warehouseService = warehouseService;
        this.categoryService = categoryService;
        this.customerService = customerService;
        this.parcelModelMapper = parcelModelMapper;
    }

    @ModelAttribute("shipments")
    public List<Shipment> getShipments() {
        return shipmentService.getAll();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> getWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("categories")
    public List<Category> getCategories() {
        return categoryService.getAll(Optional.empty());
    }

    @ModelAttribute("customers")
    public List<Customer> getCustomers() {
        return customerService.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty());
    }

    @GetMapping
    public String showAllParcels(Model model,
                                 @RequestParam("page") Optional<Integer> page,
                                 @RequestParam("size") Optional<Integer> size,
                                 HttpSession session) {

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        List<Parcel> parcels = parcelService.getAll(Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Parcel> parcelPage = parcelService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("parcelPage", parcelPage);

        int totalPages = parcelPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "parcels";
    }

    @GetMapping("/{id}")
    public String showSingleParcel(@PathVariable Long id, Model model,HttpSession session) {

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        try {
            Parcel parcel = parcelService.getById(id);
            model.addAttribute("parcel", parcel);
            return "parcel";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewParcelPage(Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }

        model.addAttribute("parcel", new ParcelDto());
        return "parcel-new";
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "parcel-new";
        }

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        try {
            Parcel parcel = parcelModelMapper.fromDto(parcelDto);
            parcelService.create(parcel);

            return "redirect:/parcels";
        } catch (LocalDateException e) {
            errors.rejectValue("parcel","parcel_error",e.getMessage());
            return "parcel-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditParcelPage(@PathVariable Long id, Model model, HttpSession session) {

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }

        try {
            Parcel parcel = parcelService.getById(id);
            ParcelDto parcelDto = parcelModelMapper.toDto(parcel);
            model.addAttribute("parcel", parcelDto);
            return "parcel-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }

    @PostMapping("/{id}/update")
    public String updateParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                                @PathVariable Long id,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return "parcel-new";
        }

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        }catch (UnauthorizedOperationException e){
            return "unauthorized";
        }

        try {

            Parcel parcel = parcelModelMapper.fromDto(parcelDto,id);
            parcelService.update(parcel);
            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteParcel(@PathVariable Long id, Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            parcelService.delete(id);
            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }catch (UnauthorizedOperationException e){
            return "unauthorized";
        }
    }

    @GetMapping("/customer/{customerId}")
    public String showTrackCustomerParcel(/*@PathVariable Long userId*/
                                          Model model,
                                          @RequestParam("page") Optional<Integer> page,
                                          @RequestParam("size") Optional<Integer> size,
                                          HttpSession session) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Customer customer;
        try{
            customer = customerService.getByUsername((String) session.getAttribute("currentUserUsername"));
        }catch (EntityNotFoundException e){
            return "not-found";
        }

        Page<Parcel> parcelPage = parcelService.findPaginated(PageRequest.of(currentPage - 1, pageSize),customer.getId());

        model.addAttribute("parcelPage", parcelPage);

        int totalPages = parcelPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        List<Parcel> customerParcels = parcelPage.getContent();

        for (Parcel p : customerParcels) {
            List<Parcel> shipmentParcels = parcelService.getShipmentParcels(p.getShipment().getId());
            p.getShipment().calcStatus(shipmentParcels);
        }

        model.addAttribute("customerParcels", customerParcels);

        return "track-customer-parcel";
    }
}
