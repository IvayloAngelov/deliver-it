package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import com.telerikacademy.web.deliverit.services.modelMappers.WarehouseModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.web.deliverit.controllers.AuthenticationHelper.EMPLOYEE_ROLE;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController extends BasicAuthenticationMvcController {

    private final WarehouseService warehouseService;
    private final CountryService countryService;
    private final CityService cityService;
    private final WarehouseModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseMvcController(WarehouseService warehouseService,
                                  CountryService countryService,
                                  CityService cityService,
                                  WarehouseModelMapper modelMapper,
                                  AuthenticationHelper authenticationHelper) {
        super(authenticationHelper);
        this.warehouseService = warehouseService;
        this.countryService = countryService;
        this.cityService = cityService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @GetMapping
    public String showAllWarehouses(Model model,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Warehouse> warehousePage = warehouseService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        List<Warehouse> warehouses = warehousePage.getContent();

        model.addAttribute("warehouses", warehouses);
        model.addAttribute("warehouseSearchDto", new WarehouseSearchDto());
        model.addAttribute("warehousePage", warehousePage);

        int totalPages = warehousePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "warehouses";
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable Long id, Model model) {
        try {
            Warehouse warehouse = warehouseService.getById(id);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/new")
    public String showNewWarehousePage(Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
        model.addAttribute("warehouse", new WarehouseDto());
        return "warehouse-new";
    }

    @PostMapping("/new")
    public String createWarehouse(@Valid
                                  @ModelAttribute("warehouse") WarehouseDto warehouseDto,
                                  BindingResult errors,
                                  Model model,
                                  HttpSession session) {

        if (errors.hasErrors()) {
            return "warehouse-new";
        }

        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            Warehouse warehouse = modelMapper.fromDto(warehouseDto);
            warehouseService.create(warehouse);
            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("address.street", "duplicated_street", e.getMessage());
            return "warehouse-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditWarehouse(@PathVariable Long id, Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            Warehouse warehouse = warehouseService.getById(id);
            WarehouseDto warehouseDto = modelMapper.toDto(warehouse);
            model.addAttribute("warehouse", warehouseDto);
            return "warehouse-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @PostMapping("/{id}/update")
    public String editWarehouse(@PathVariable Long id,
                                @Valid @ModelAttribute("warehouse") WarehouseDto warehouseDto,
                                BindingResult errors, Model model) {

        if (errors.hasErrors()) {
            return "warehouse-update";
        }

        try {
            Warehouse warehouse = modelMapper.fromDto(warehouseDto, id);
            warehouseService.update(warehouse);
            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("address.street", "duplicate_street", e.getMessage());
            return "warehouse-update";
        }
    }


    @GetMapping("/{id}/delete")
    public String deleteWarehouse(@PathVariable Long id, Model model, HttpSession session) {
        try {
            authenticationHelper.verifyAuthorization(session, EMPLOYEE_ROLE);
            warehouseService.delete(id);
            return "redirect:/warehouses";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DeletingEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "delete-warning";
        }
    }

    @GetMapping("/search")
    public String handleSearchGetWarehouses() {
        return "redirect:/warehouses";
    }

    @PostMapping("/search")
    public String handleWarehouseSearch(Model model,
                                        @ModelAttribute("warehouseSearchDto") WarehouseSearchDto warehouseSearchDto) {
        model.addAttribute("warehouses", warehouseService.filter(modelMapper.fromDto(warehouseSearchDto)));

        return "warehouses";
    }
}
