package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.CustomerDto;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.services.modelMappers.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/customers")
public class CustomerController {

    private final CustomerService service;
    private final CustomerModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CustomerController(CustomerService service, CustomerModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Customer> getAll(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) Optional<String> firstName,
                                 @RequestParam(required = false) Optional<String> lastName,
                                 @RequestParam(required = false) Optional<String> email) {
        authenticationHelper.tryGetEmployee(headers);
        return service.getAll(firstName, lastName, email);
    }

    @GetMapping("/numberOfCustomers")
    public Integer getAll(@RequestParam(required = false) Optional<String> firstName,
                          @RequestParam(required = false) Optional<String> lastName,
                          @RequestParam(required = false) Optional<String> email) {
        return service.getAll(firstName, lastName, email).size();
    }

    @GetMapping("/{id}")
    public Customer getById(@RequestHeader HttpHeaders headers,@PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers);
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/username/{username}")
    public Customer getByUsername(@RequestHeader HttpHeaders headers,@PathVariable String username) {
        try {
            authenticationHelper.tryGetCustomer(headers,username);
            return service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/email/{email}")
    public Customer getByEmail(@RequestHeader HttpHeaders headers,@PathVariable String email) {
        try {
            authenticationHelper.tryGetCustomer(headers,email);
            return service.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public Customer create(@Valid @RequestBody CustomerDto customerDto) {
        try {
            Customer customer = modelMapper.fromDto(customerDto);
            return service.create(customer);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Customer update(@RequestHeader HttpHeaders headers,@PathVariable Long id, @Valid @RequestBody CustomerDto customerDto) {
        try {
            authenticationHelper.tryGetCustomer(headers,id);
            Customer customer = modelMapper.fromDto(customerDto, id);
            return service.update(customer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetCustomer(headers,id);
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
