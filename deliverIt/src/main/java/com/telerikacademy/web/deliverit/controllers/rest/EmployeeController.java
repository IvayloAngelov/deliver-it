package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.EmployeeDto;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import com.telerikacademy.web.deliverit.services.modelMappers.EmployeeModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/employees")
public class EmployeeController {

    private final EmployeeService service;
    private final EmployeeModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeController(EmployeeService service, EmployeeModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Employee> getAll(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) Optional<String> username,
                                 @RequestParam(required = false) Optional<String> email,
                                 @RequestParam(required = false) Optional<String> firstName,
                                 @RequestParam(required = false) Optional<String> lastName) {
        authenticationHelper.tryGetManager(headers);
        return service.getAll(username,email,firstName, lastName);
    }

    @GetMapping("/{id}")
    public Employee getById(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetEmployee(headers, id);
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/username/{username}")
    public Employee getByUsername(@RequestHeader HttpHeaders headers,@PathVariable String username) {
        try {
            authenticationHelper.tryGetEmployee(headers, username);
            return service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/email/{email}")
    public Employee getByEmail(@RequestHeader HttpHeaders headers,@PathVariable String email) {
        try {
            authenticationHelper.tryGetEmployee(headers, email);
            return service.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    public Employee create(@RequestHeader HttpHeaders headers, @Valid @RequestBody EmployeeDto employeeDto) {
        try {
            authenticationHelper.tryGetManager(headers);
            Employee employee = modelMapper.fromDto(employeeDto);
            return service.create(employee);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Employee update(@RequestHeader HttpHeaders headers, @PathVariable Long id, @Valid @RequestBody EmployeeDto employeeDto) {
        try {
            authenticationHelper.tryGetEmployee(headers, id);
            Employee employee = modelMapper.fromDto(employeeDto, id);
            return service.update(employee);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Long id) {
        try {
            authenticationHelper.tryGetManagerOrEmployee(headers,id);
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}