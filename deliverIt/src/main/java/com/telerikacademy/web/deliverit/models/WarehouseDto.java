package com.telerikacademy.web.deliverit.models;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

public class WarehouseDto {

    @Valid
    private AddressDto address;

    public WarehouseDto() {
    }

    public WarehouseDto(AddressDto address) {
        setAddress(address);
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto addressDto) {
        this.address = addressDto;
    }
}
