package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.models.Role;
import com.telerikacademy.web.deliverit.repositories.contracts.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Role", Role.class)
                    .list();
        }
    }

    @Override
    public Role getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Role  where name = :name", Role.class)
                    .setParameter("name", name)
                    .uniqueResult();
        }
    }
}
