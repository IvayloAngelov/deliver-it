package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Employee;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {
    List<Employee> getAll (Optional<String> username,
                           Optional<String> email,
                           Optional<String> firstName,
                           Optional<String> lastName);


    Employee getById(Long id);

    Employee getByEmail(String email);

    Employee getByUsername(String username);

    Employee create(Employee employee);

    Employee update(Employee employee);

    Employee delete(Long id);

}
