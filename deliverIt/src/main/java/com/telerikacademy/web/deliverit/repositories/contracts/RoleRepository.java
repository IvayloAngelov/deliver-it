package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Role;

import java.util.List;

public interface RoleRepository {

    List<Role> getAll();

    Role getByName(String name);

}
