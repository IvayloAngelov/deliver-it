package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository repository;

    @Autowired
    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<City> getAll() {
        return repository.getAll();
    }

    @Override
    public City getById(Long id) {
        return repository.getById(id);
    }


    @Override
    public City getByName(String name) {
        return repository.getByName(name);
    }


}
