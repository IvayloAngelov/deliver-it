package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ParcelServiceImpl implements ParcelService {

    private final ParcelRepository parcelRepository;

    @Autowired
    public ParcelServiceImpl(ParcelRepository parcelRepository) {
        this.parcelRepository = parcelRepository;
    }

    @Override
    public List<Parcel> getAll(@RequestParam(required = false) Optional<String> customerUsername,
                               @RequestParam(required = false) Optional<String> warehouseCityName,
                               @RequestParam(required = false) Optional<Double> weight,
                               @RequestParam(required = false) Optional<String> category,
                               @RequestParam(required = false) Optional<Boolean> orderByWeight,
                               @RequestParam(required = false) Optional<Boolean> orderByDepartureDate) {
        return parcelRepository.getAll(customerUsername,
                warehouseCityName,
                weight,
                category,
                orderByWeight,
                orderByDepartureDate);
    }

    @Override
    public List<Parcel> getShipmentParcels(Long shipmentId) {
        return parcelRepository.getShipmentParcels(shipmentId);
    }

    @Override
    public List<Parcel> getCustomerParcels(Long customerId) {
        return parcelRepository.getCustomerParcels(customerId);
    }

    @Override
    public Parcel getById(Long id) {
        return parcelRepository.getById(id);
    }

    @Override
    public Parcel getById(Long id, Customer customer) {
        Parcel parcel = parcelRepository.getById(id);
        if (!parcel.getCustomer().getId().equals(customer.getId())) {
            throw new UnauthorizedOperationException("Unauthorized to get this parcel!");
        }
        return parcel;
    }

    @Override
    public Parcel create(Parcel parcel) {

        List<Parcel> parcels = getShipmentParcels(parcel.getShipment().getId());
        parcel.getShipment().calcStatus(parcels);

        if (parcel.getShipment().getStatus().equals(Status.COMPLETED)) {
            throw new LocalDateException("Cannot add a parcel with shipment status 'Completed'");
        } else if (parcel.getShipment().getStatus().equals(Status.ONTHEWAY)) {
            throw new LocalDateException("Shipment is on the way! You can't add a parcel to shipment with status on the way!");
        }

        return parcelRepository.create(parcel);
    }

    @Override
    public Parcel update(Parcel parcel) {
        return parcelRepository.update(parcel);
    }

    @Override
    public void delete(Long id) {

        Parcel parcel = getById(id);
        List<Parcel> parcels = getShipmentParcels(parcel.getShipment().getId());

        parcel.getShipment().calcStatus(parcels);

        if (parcel.getShipment().getStatus().equals(Status.ONTHEWAY)) {
            throw new DeletingEntityException("Cannot delete a parcel with shipment status 'On The Way'");
        }
        parcelRepository.delete(id);
    }

    @Override
    public Page<Parcel> findPaginated(Pageable pageable) {

        List<Parcel> allParcels = getAll(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty());

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Parcel> list;

        if (allParcels.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allParcels.size());
            list = allParcels.subList(startItem, toIndex);
        }

        Page<Parcel> parcelPage
                = new PageImpl<Parcel>(list, PageRequest.of(currentPage, pageSize), allParcels.size());

        return parcelPage;
    }

    @Override
    public Page<Parcel> findPaginated(Pageable pageable,Long customerId) {

        List<Parcel> allCustomerParcels = getCustomerParcels(customerId);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Parcel> list;

        if (allCustomerParcels.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allCustomerParcels.size());
            list = allCustomerParcels.subList(startItem, toIndex);
        }

        return new PageImpl<Parcel>(list, PageRequest.of(currentPage, pageSize), allCustomerParcels.size());
    }
}
