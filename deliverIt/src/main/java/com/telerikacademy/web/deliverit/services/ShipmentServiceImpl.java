package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.LocalDateException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.ShipmentSearchParameters;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.repositories.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    public static final String FIST_DELETE_PARCELS_ERROR_MSG = "You have to remove the parcels first.";
    private final ShipmentRepository repository;
    private final ParcelService parcelService;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository repository,
                               ParcelService parcelService) {
        this.repository = repository;
        this.parcelService = parcelService;
    }

    @Override
    public List<Shipment> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Shipment> filter(ShipmentSearchParameters ssp) {
        return repository.filter(ssp);
    }

    @Override
    public Shipment getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public List<Parcel> getShipmentParcels(Long shipmentId) {
        return parcelService.getShipmentParcels(shipmentId);
    }

    @Override
    public Shipment create(Shipment shipment) {

        if (shipment.getDepartureDate().isBefore(LocalDate.now())) {
            throw new LocalDateException("Invalid departure date! Cannot create a shipment in the past.");
        }

        if (shipment.getArrivalDate().isBefore(LocalDate.now())) {
            throw new LocalDateException("Invalid arrival date! Cannot create a shipment in the past.");
        }

        if (shipment.getArrivalDate().isBefore(shipment.getDepartureDate())) {
            throw new LocalDateException("Invalid arrival date!");
        }

        return repository.create(shipment);
    }

    @Override
    public void getShipmentStatus(Shipment shipment) {
        List<Parcel> parcels = parcelService.getShipmentParcels(shipment.getId());
        shipment.calcStatus(parcels);
    }

    @Override
    public Shipment update(Shipment shipment) {
        Shipment shipmentToUpdate = getById(shipment.getId());
        List<Parcel> parcels = parcelService.getShipmentParcels(shipmentToUpdate.getId());
        shipmentToUpdate.calcStatus(parcels);

        if (shipmentToUpdate.getStatus().equals(Status.ONTHEWAY)) {
            throw new LocalDateException("Cannot update a shipment with status 'On The Way");
        } else if (shipmentToUpdate.getStatus().equals(Status.COMPLETED)) {
            throw new LocalDateException("Cannot update a shipment with status 'Completed");
        }

        if (shipment.getArrivalDate().isBefore(shipment.getDepartureDate())) {
            throw new LocalDateException("Invalid arrival date!");
        }

        if (shipment.getDepartureDate().isBefore(LocalDate.now())) {
            throw new LocalDateException("Invalid departure date!");
        }

        if (shipment.getArrivalDate().isBefore(LocalDate.now())) {
            throw new LocalDateException("Invalid arrival date!");
        }

        return repository.update(shipment);
}

    @Override
    public void delete(Long id) {

        Shipment shipmentToDelete = getById(id);
        List<Parcel> parcels = parcelService.getShipmentParcels(shipmentToDelete.getId());
        shipmentToDelete.calcStatus(parcels);

        if (shipmentToDelete.getStatus().equals(Status.ONTHEWAY)) {
            throw new DeletingEntityException("Cannot delete a shipment with status 'On The Way'.");
        }

        if (parcels.size() != 0) {
            throw new DeletingEntityException(FIST_DELETE_PARCELS_ERROR_MSG);
        }

        repository.delete(shipmentToDelete);
    }

    @Override
    public Page<Shipment> findPaginated(Pageable pageable) {

        List<Shipment> allShipments = getAll();

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Shipment> list;

        if (allShipments.size()< startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allShipments.size());
            list = allShipments.subList(startItem, toIndex);
        }

        Page<Shipment> shipmentPage
                = new PageImpl<Shipment>(list, PageRequest.of(currentPage, pageSize), allShipments.size());

        return shipmentPage;
    }

}
