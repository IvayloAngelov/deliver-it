package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.City;

import java.util.List;
import java.util.Optional;

public interface CityService {
    List<City> getAll();

    City getById(Long id);

    City getByName(String name);
}
