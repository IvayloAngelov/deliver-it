package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    List<Employee> getAll(Optional<String> username,
                          Optional<String> email,
                          Optional<String> firstName,
                          Optional<String> lastName);

    Employee getById(Long id);

    Employee getByUsername(String username);

    Employee getByEmail(String email);

    Employee create(Employee employee);

    Employee update(Employee employee);

    Employee delete(Long id);

    Page<Employee> findPaginated(Pageable pageable);
}
