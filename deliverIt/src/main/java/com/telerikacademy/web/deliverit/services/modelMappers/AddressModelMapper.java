package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.AddressDto;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressModelMapper {
    private final AddressRepository repository;
    private final CityRepository cityRepository;

    @Autowired
    public AddressModelMapper(AddressRepository repository, CityRepository cityRepository) {
        this.repository = repository;
        this.cityRepository = cityRepository;
    }

    public Address fromDto(AddressDto addressDto) {
        Address address = new Address();
        dtoToObject(addressDto, address);
        return address;
    }

    public AddressDto toDto(Address address) {
        AddressDto addressDto = new AddressDto();
        addressDto.setStreet(address.getStreet());
        addressDto.setCityId(address.getCity().getId());
        return addressDto;
    }

    public Address fromDto(AddressDto addressDto, Long id) {
        Address address = repository.getById(id);
        dtoToObject(addressDto, address);
        return address;
    }

    private void dtoToObject(AddressDto addressDto, Address address) {
        City city = cityRepository.getById(addressDto.getCityId());
        address.setCity(city);
        address.setStreet(addressDto.getStreet());
    }

}
