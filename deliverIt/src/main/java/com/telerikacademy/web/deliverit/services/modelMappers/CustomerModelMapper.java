package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class CustomerModelMapper {
    private final CustomerRepository customerRepository;
    private final UserModelMapper userModelMapper;
    private final AddressModelMapper addressModelMapper;
    private final RoleRepository roleRepository;

    @Autowired
    public CustomerModelMapper(CustomerRepository customerRepository,
                               UserModelMapper userModelMapper,
                               AddressModelMapper addressModelMapper,
                               RoleRepository roleRepository) {
        this.customerRepository = customerRepository;
        this.userModelMapper = userModelMapper;
        this.addressModelMapper = addressModelMapper;
        this.roleRepository = roleRepository;
    }

    public Customer fromDto(CustomerDto customerDto) {
        Customer customer = new Customer();
        dtoToObject(customerDto, customer);

        return customer;
    }

    public Customer fromDto(CustomerDto customerDto, Long id) {
        Customer customer = customerRepository.getById(id);
        dtoToObjectUpdate(customerDto, customer);
        return customer;
    }

    private void dtoToObject(CustomerDto customerDto, Customer customer) {
        User user = userModelMapper.fromDto(customerDto.getUser());
        Address address = addressModelMapper.fromDto(customerDto.getAddress());
        customer.setUser(user);
        customer.setAddress(address);
        customer.getUser().setRoles(Set.of(roleRepository.getByName("Customer")));
    }

    private void dtoToObjectUpdate(CustomerDto customerDto, Customer customer) {
        User user = userModelMapper.fromDto(customerDto.getUser(),customer.getUser().getId());
        Address address = addressModelMapper.fromDto(customerDto.getAddress(),customer.getAddress().getId());
        customer.setUser(user);
        customer.setAddress(address);
    }

    public CustomerDto toDto(Customer customer){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setUser(userModelMapper.toDto(customer.getUser()));
        customerDto.setAddress(addressModelMapper.toDto(customer.getAddress()));
        return customerDto;
    }

    public Customer fromRegisterDto(RegisterDto registerDto){
        Customer customer = new Customer();
        registerDtoToObject(customer,registerDto);
        customer.getUser().setRoles(Set.of(roleRepository.getByName("Customer")));
        return  customer;
    }

    public void registerDtoToObject(Customer customer, RegisterDto registerDto){
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        Address address = addressModelMapper.fromDto(registerDto.getAddressDto());
        customer.setUser(user);
        customer.setAddress(address);
    }
}

