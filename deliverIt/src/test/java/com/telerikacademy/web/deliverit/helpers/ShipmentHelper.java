package com.telerikacademy.web.deliverit.helpers;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.enums.Status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.web.deliverit.helpers.ParcelHelper.createMockParcel;

public class ShipmentHelper {

    public static Shipment createMockShipment() {
        var mockShipment = new Shipment();
        mockShipment.setId(1L);
        mockShipment.setDepartureDate(LocalDate.of(2021, 03, 10));
        mockShipment.setArrivalDate(LocalDate.of(2021, 03, 20));
        mockShipment.setStatus(Status.PREPARING);
        return mockShipment;
    }

    public static List<Parcel> getParcelsToShipment(){
        List<Parcel> parcelsOfShipment = new ArrayList<>();
        Parcel parcel = createMockParcel();
        parcel.setId(5L);
        parcelsOfShipment.add(parcel);
        parcel = createMockParcel();
        parcel.setId(6L);
        parcelsOfShipment.add(parcel);
        return parcelsOfShipment;
    }

}
