package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.repositories.contracts.CategoryRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.CountryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTests {
    @Mock
    CountryRepository mockRepository;

    @InjectMocks
    CountryServiceImpl service;

    private Country country;

    @BeforeEach
    public void setInstance() {
        country = new Country();
        country.setId(1L);
        country.setName("Bulgaria");
    }


    @Test
    public void getAll_Should_Call_Repository() {

        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        verify(mockRepository, times(1)).getAll();
    }

    @Test
    public void getById_Should_Call_Repository() {

        when(mockRepository.getById(country.getId()))
                .thenReturn(country);

        Assertions.assertEquals(country, service.getById(country.getId()));
    }

    @Test
    public void getByName_Should_Call_Repository() {

        when(mockRepository.getByName(country.getName()))
                .thenReturn(country);

        Assertions.assertEquals(country, service.getByName(country.getName()));
    }
}
